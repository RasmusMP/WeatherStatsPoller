﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modules;
using System.IO;

namespace WeatherStats.Modules
{
    public class ImportOffline : ModuleBase
    {
        public ImportOffline(string run) : base(nameof(ImportOffline))
        {
            this.Run = run;
        }

        public string Run;

        protected override void DoWork()
        {

        }

        protected override async Task DoWork2()
        {
            var offlinePath = System.Configuration.ConfigurationManager.AppSettings["OfflineStoragePath"];
            //Console.WriteLine("Path " + offlinePath);

            if (this.Run == "Run")
            {
                string[] offlineFiles = Directory.GetFiles(offlinePath);
                foreach (string fileName in offlineFiles)
                    ProcessFile(fileName);
            }

        }

        public void ProcessFile(string path)
        {
            Console.WriteLine("Processed file '{0}'.", path);

                // Open the stream and read it back.
                using (FileStream fs = File.Open(path, FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    byte[] b = new byte[1024];
                    UTF8Encoding temp = new UTF8Encoding(true);

                    while (fs.Read(b, 0, b.Length) > 0)
                    {
                        Console.WriteLine(temp.GetString(b));
                    }
                }

        }
    }
}
